

## Go Basic Knowledge

	1) Go only has 25 keywords
		Keywords: 
			break
			default
			func
			interface
			select
			case
			defer
			go
			map
			struct
			chan
			else
			goto
			package
			switch
			const
			fallthrough
			if
			range
			type
			continue
			for
			import
			return
			var

	2) Importing packages: when importing use import "<full path>." This removes
		confusion around where the package came from

	3) syntax for calling library specific functions: <pkgName>.<funcName> 

	4) Define Variables
		- The keyword `var` is the basic form to define variables
		EX.1
			// defined variable with name variableName and type (type = int, string, ect)
			var variableName <type>

		EX.2 Define multiple variables 
			// defines three variables with type <type>
			var vname1, vname2, vname3 <type> 

		EX.3 Define variable with initial value
			// define var with value <value>
			var variableName type = value

		EX.4 Define multiple variables with initial values
			/* 
				Define three variables with type <type>, and initialize their values
				vname1 is v1, vname2 is v2, vname3 is v3
			*/ 
			var vname1, vname2, vname3 <type> = v1, v2, v3
			// can be done without declaring <type>

		EX.5 Define multiple variables with inital values without <var>
			// define 1-3 and initialize values difference is <:=> semi-colon equals
			vname1, vname2, vname3 := v1, v2, v3 
			// also called short assignment, and can only be used INSIDE of funcs

		EX.6 < _ > Blank special variable assignment
			// Any value that is given to < _ > the blank assignment with be ignored 
			_, b := 34, 35 // 34 will be discarded

	5) Constants 
		- Constants are the values that are determined during compile time, and cannot
		  change during runtime. Numbers, Booleans, or Strings can be used as constants 

		EX.1 Defined Constant
			const constantName = value
			// you can assign type of constants if it's necessary
			const Pi float32 = 3.1415926

	6) Elementary Types
		A. Boolean 
			- `bool` is used to define the boolean type, whos value can be true or false
			  false is the default value of bool types 

			  EX.1
			  	var isActive bool // global variable 
				var enabled, disabled = true, false //omit type of vars

				func test() { 
					var available bool  // local variable
					valid := false		// brief statement of variable
					available = true 	// assign value to variable 
				}
		B. Numerical: Integer Types
			- signed and unsigned integer types (int or uint) at the same time, they have
			  same length, but specific length depends on the OP system. Specific Length
			  int/uint types include: 
			  	1. rune, int8, int16, int64, byte, uint8, uint16, uint32, uint64
			  	 	- rune is an alias of int32, and byte is an alias of uint8
					// You cannot assign values between these types, this will cause compile errors

			- float types: 
				1. float32, float64
					- There is no type called float

			- Complex Numbers: 
				1. complex128, complex64
					- complex128 contains 64-bit real and 64-bit imaginary part: default
					- complex64 containes 32-bit real and 32-bit imaginary part
				
				EX.1
					var c complex64 = 5+5i
					// output: (5+5i) 
					fmt.Printf("Value is: %v, c)

		C. Strings
			- Strings are represented by double quotes "" or backticks ``. It is impossible to change
			  string values by index, the code won't compile. 

			EX.1
				var frenchHello string			// basic form to define string
				var emptyString string = ""		// define an empty string 
				func test() { 
					no, yes, maybe := "no", "yes", "maybe" // brief statement
					japaneseHello := "Ohaiou"
					frenchHello = "Bonjour"				  // basic form of assign values

			EX.2 String Character Change
				s := "hello"
				c := []byte(s)		// convert string to []byte type
				c[0] = 'c'
				s2 := string(c)		// convert back to string type
				fmt.Printf("%s\n", s2) 

			EX.3 Combining Strings
				s := "hello,"
				m := " world"
				a := s + m 
				fmt.Printf("%s\n", a)

			EX.4 Multiple Line Strings 
				m := `hello
					world` // ` will not escape any characters in a string

		D. Error Types 
			- Go has one <error> type for purpose of dealing with error messages. There is 
			  also a package called "errors" to handle errors. 

			EX.1 
				err := errors.New("emit macho dwarf: elf header corrupted") 
				if err != nil {
					fmt.Print(err)
				} 

		E. Underlying Data Structure 
			- Go uses blocks of memory to store data: Check (Go foundation for more details)

		F. Define by Group
			- If you want to define multiple constants, variables or import packages, use 
			  group form. 

			EX.1 Group Form 
				import(
					"fmt"
					"os"
				)

				const(
					i = 100
					pi = 3.1415
					prefix = "Go_"
				) // Unless you assign the value of constant as <iota> the first value of the 
				  // const group will be zero. 
				  // If the value of the last constant is <iota>, the values following constants
				  // which are not assigned are <iota> also. 

				var(
					i int
					pi float32
					prefix string
				) 

		G. iota Enumerate
			- Go has one keyword called <iota>, this keyword is to make enum, it begins with zero, 
			  increased by one. 

			EX.1 
				const(
					x = iota		// x == 0
					y = iota		// y == 0
					z = iota		// z == 0
					w				// if there is no expression after the constants name, it uses
									  // the last expression so <w = iota> implicitly, so <w == 3>

		H. Array
			- An array is an array obviously defined below

			EX.1 
				var arr [n]type 
				// <[n]type> n is the length of the array
				// type is the type of its elements
				// <[]> to get or set element values within arrays

			EX.2
				var arr [10]init		// an array of type [10]int
				arr[0] = 42				// array is 0-based
				arr[1] = 13				// assign value to element

				fmt.Printf("The first element is %d\n", arr[0])  // returns 42

				fmt.Printf("The last element is %d\n", arr[9]) 
				// returns default value of 10th element in this array, which is zero in this case

			- <[3]int> and <[4]int> are different types, so we can't change the length of the array
			  You can use functions to get copies instead of references. If you want to use 
			  references you will want to use <slice>. 

			EX.3 
				a := [3]int{1, 2, 3} // define an int array with 3 elements

				b := [10]int{1, 2, 3} // define a int array with 10 elements, of which the first 
									  // three are assigned, the remainder values are zero. 

				c := [...]int{4, 5, 6} // use <...> to replace the length parameter (auto calculates)

			EX.4 Using Arrays as elements
				// define a two-dimensional array with 2 elements, and each element has 4 elements
				doubleArray := [2][4]int{[4]int{1, 2, 3, 4}, [4]int{5, 6, 7, 8}} 

				// Double Array can be more written more conisely as follows
				easyDoubleArray := [2][4]int{{1, 2, 3, 4}, {5, 6, 7, 8}}

			- See Multidimensional array mapping relationship (IMG)

		I. Slice
			- If you don't know how long the array will be then you'll need to define a "dynamic 
			  array," also called a slice in Go. 
			- A Slice is not really a dynamic array, its a reference type <slice> pointing to an underlying
			  <array> whose declaration is similar to array, but doesn't need length. 

			EX.1 
				// just like defining an array but this time, we exclude the length
				var fslice []int 

			EX.2 Define a Slice and Initialize its data
				slice := []byte {'a', 'b', 'c', 'd'} 

			- Slice can redefine an existing slices or arrays. Slice uses <array[i:j] to slice from 
			  <i> or the starting location to <j> the end of its index. 
			- Notice that array[j] will not be sliced sence the length of the slice is j-i 

			EX.3 
				// define an array with 10 elements who's types are bytes 
				var ar = [10]byte {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'} 

				// define two slices with type []byte 
				var a, b []byte 

				// 'a' points to elements from the 3rd to 5th in array <ar> 
				a = ar[2:5] // now 'a' has elements ar[2], ar[3], and ar[4] 

				// 'b' is another slice of array ar 
				b = ar[3:5] // now 'b' has elements ar[3], and ar[4] 

			EX.4 Slice Examples
				// define an array
				var array = [10]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'}
				// define two slices 
				var aSlice, bSlice []byte 

				// conveniet operations 
				aSlice = array[:3] // equals to aSlice = array[0:5], with elements a,b,c
				aSlice = array[5:] // equal to aSlice = array[5:10], with elements f,g,h,i,j
				aSlice = array[:]  // equal to aSlice = array[0:10], with all elements

				// slice from slice 
				aSlice = array[3:7]  // aSlice has elements d,e,f,g，len=4，cap=7
				bSlice = aSlice[1:3] // bSlice contains aSlice[1], aSlice[2], so it has elements e,f
				bSlice = aSlice[:3]  // bSlice contains aSlice[0], aSlice[1], aSlice[2], so it has d,e,f
				bSlice = aSlice[0:5] // slice could be expanded in range of cap, now bSlice contains d,e,f,g,h
				bSlice = aSlice[:]   // bSlice has same elements as aSlice does, which are d,e,f,g

			- slice is a reference type, so any changes will affect other variables pointing to the same 
			  slice or array. 
			- The above example when changing aSlice and bSlice, if you change the value of an element in 
			  aSlice, bSlice will be changed as well. 

		J. Map
			- <map> behaves like a dictionary in python. Using the form: 

					map[keyType]valueType
			  
			  to define a map. The 'set' and 'get' values in <map> are similar to <slice>, however the index
			  in <slice> can only be type <int>, while map can use much more that that. 
			- Map can use types: int, string, or whatever, and can use <==> or <!=> to compare values. 

			EX.1 
				// use string as the key type, int as the value type, and `make` initialize it.
				var numbers map[string] int

				// another way to define map
				numbers := make(map[string]int)
				numbers["one"] = 1  // assign value by key
				numbers["ten"] = 10
				numbers["three"] = 3

				fmt.Println("The third number is: ", numbers["three"]) // get values
				// It prints: The third number is: 3

			- Map is disorderly, everytime you print map, you will get different results. Its impossible
			  to get values by index - you have to use <key> 
			- map doesn't fix length, its just a reference type like slice 
			- <len> works for map. It returns how many <key>s that map has. 
			- To change the value through map use: 

				numbers["one"]=11 

			  to change the value of key one to 11. 
			- You can use form <key:val> to initialize map's values, and map has built in methods to check
			  if <key> exists. 

			EX.2 Use delete to delete an element in <map> 
				// Initialize a map 
				rating := map[string]float32 {"C":5, "Go":4.5, "Python":4.5, "C++":2}
				// map has two return values. for the second return value, if the key doesn't
				// exist, 'ok' returns false. It returns <true> otherwise. 
				csharpRating, ok := rating["C#"] 
				if ok {
					fmt.Printf("C# is in the map and its rating is ", csharpRating) 
				} else {
					fmt.Printf("We have no rating associated with C# in the map.")
				} 

				delete(rating, "C") // delete element with key "c"

			- Because <map> is a reference type. If two <map>s point to the same underlying data, any change
			  will affect both of them. 

			EX.3 Duel Changes 
				m := make(map[string]string) 
				m["hello"] = "Bonjour" 
				m1 := m 
				m1["hello"] = "salut" // now the value of m["hello"] is salut. 

		k. Make, New 
			- <make> does memory allocation for built in models such as <map>, <slice>, and <channel> while
			  <new> is for types' memory allocation. 
			- <new(T)> allocates zero-value to type <T>'s memory, returns its memory address, which is the 
			  value of type <*T>. Returning a pointer which points to type <T>'s zero-value. 
			- <new> returns pointers. 
			- Built-in function <make(T, args)> have different purposes than <new(T)>. 
			
			- The reason for doing this is because the underlying data of these three types must be initialzed
			  before they point to them. 
			  	a. For example, a <slice> contains a pointer that points to the underlying <array>, length and 
				   capacity. Before these data are initialized, <slice>, is nil, so for <slice>, <map>, and 
				   <channel>, <MAKE> initializes their underlying data and assigns some suitable values. 

			- <make> returns NON-ZERO values.

			- Zero-value does not mean empty value. It's the value that vars default to in most cases. 

			EX.1 List of Zero values
				- int/8/32/64		0
				- uint				0x0
				- rune				0 // the actual type of run is int32
				- byte				0x0 //the actual type  of byte is uint8
				- float32/64		0 
				- bool				false
				- string			""
		


































